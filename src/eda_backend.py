import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA, TruncatedSVD
import matplotlib.patches as mpatches
from pathlib import Path
import streamlit as st
import matplotlib.pyplot as plt
from src.constants import INPUT_PATH,OUTPUT_PATH,DATASET_PATH
from sklearn.preprocessing import StandardScaler, RobustScaler
class analyse:
	def __init__(self):
		self.dataset = pd.read_csv(self.getDataset())
		
	def getDataset(self):
		#constantsPath = Path(__file__).parent / 'constants'
		return INPUT_PATH / 'creditcard.csv'

	def getPlotsFolder(self):
		return OUTPUT_PATH / 'plots'

	def drawColumnChart(self):
		colors = ["#0101DF", "#DF0101"]
		sns.countplot('Class', data=self.dataset, palette=colors)
		plt.title('Class Distribution', fontsize=14)
		charPath = self.getPlotsFolder() /'ColumnChart.png'
		plt.savefig(charPath)
		st.image(str(charPath))

	def drawAmountDistribution(self):
		fig, ax = plt.subplots()
		amount_val = self.dataset['Amount'].values
		sns.distplot(amount_val, ax=ax, color='r')
		ax.set_title('Transaction Amount Distribution', fontsize=14)
		ax.set_xlim([min(amount_val), max(amount_val)])
		charPath = self.getPlotsFolder() / 'AmountDistribution.png'
		plt.savefig(str(charPath))
		st.image(str(charPath))

	def drawTimeDistribution(self):
		fig, ax = plt.subplots()
		time_val = self.dataset['Time'].values
		sns.distplot(time_val, ax=ax, color='r')
		ax.set_title('Transaction Time Distribution', fontsize=14)
		ax.set_xlim([min(time_val), max(time_val)])
		charPath = self.getPlotsFolder() / 'TimeDistribution.png'
		plt.savefig(str(charPath))
		st.image(str(charPath))

	def scaleData(self):
		# RobustScaler is less prone to outliers.
		robustScaler = RobustScaler()
		try:
			self.dataset['scaled_amount'] = robustScaler.fit_transform(self.dataset['Amount'].values.reshape(-1,1))
			self.dataset['scaled_time'] = robustScaler.fit_transform(self.dataset['Time'].values.reshape(-1,1))
			self.dataset.drop(['Time','Amount'], axis=1, inplace=True)
		except Exception as e:
			print('couldn\'t scale data')

		scaled_amount = self.dataset['scaled_amount']
		scaled_time = self.dataset['scaled_time']
		try:
			self.dataset.drop(['scaled_amount', 'scaled_time'], axis=1, inplace=True)
			self.dataset.insert(0, 'scaled_amount', scaled_amount)
			self.dataset.insert(1, 'scaled_time', scaled_time)
		except Exception as e:
			print('couldn\'t insert data')
	def varryingDataset(self):
		# Lets shuffle the data before creating the subsamples

		fractionnedDataset = self.dataset.sample(frac=1)

		# amount of fraud classes 492 rows.
		fraudDataset = fractionnedDataset.loc[fractionnedDataset['Class'] == 1]
		noneFraudDataset = fractionnedDataset.loc[fractionnedDataset['Class'] == 0][:492]
		try:
			reDistributedDataset = pd.concat([fraudDataset, noneFraudDataset])
		except Exception as e:
			print('couldn\'t concatenate data')	
		# Shuffle dataframe rows
		self.finalDataset = reDistributedDataset.sample(frac=1, random_state=42)

	def drawNewDistribution(self):
		sns.countplot('Class', data=self.finalDataset)
		plt.title('Equally Distributed Classes', fontsize=14)
		charPath = self.getPlotsFolder() / 'newDistribution.png'
		plt.savefig(str(charPath))
		st.image(str(charPath))

	def drawCorrelationMatrix(self):
		f, ax = plt.subplots()

		# Entire dataset
		corr = self.dataset.corr()
		sns.heatmap(corr, cmap='coolwarm_r', ax=ax)
		ax.set_title("original Correlation Matrix ", fontsize=14)

		plotPath = self.getPlotsFolder() / 'oldCorrelationMatrix.png'
		plt.savefig(str(plotPath))
		st.image(str(plotPath))

		#subset of dataset
		sub_sample_corr = self.finalDataset.corr()
		sns.heatmap(sub_sample_corr, cmap='coolwarm_r' , ax=ax)
		ax.set_title('SubSample Correlation Matrix ', fontsize=14)

		plotPath = self.getPlotsFolder() / 'newCorrelationMatrix.png'
		plt.savefig(str(plotPath))
		st.image(str(plotPath))


	def drawNegativeCorrelations(self):
		f, axes = plt.subplots(ncols=4, figsize=(20,10))

		# Negative Correlations with our Class (The lower our feature value the more likely it will be a fraud transaction)
		sns.boxplot(x="Class", y="V17", data=self.finalDataset, ax=axes[0])
		axes[0].set_title('V17 vs Class Negative Correlation')

		sns.boxplot(x="Class", y="V14", data=self.finalDataset, ax=axes[1])
		axes[1].set_title('V14 vs Class Negative Correlation')


		sns.boxplot(x="Class", y="V12", data=self.finalDataset, ax=axes[2])
		axes[2].set_title('V12 vs Class Negative Correlation')


		sns.boxplot(x="Class", y="V10", data=self.finalDataset, ax=axes[3])
		axes[3].set_title('V10 vs Class Negative Correlation')

		plotPath = self.getPlotsFolder() / 'negativeCorrelations.png'
		plt.savefig(str(plotPath))
		st.image(str(plotPath))

	def drawPositiveCorrelations(self):
		f, axes = plt.subplots(ncols=4, figsize=(20,10))

		# Positive correlations (The higher the feature the probability increases that it will be a fraud transaction)
		sns.boxplot(x="Class", y="V11", data=self.finalDataset,  ax=axes[0])
		axes[0].set_title('V11 vs Class Positive Correlation')

		sns.boxplot(x="Class", y="V4", data=self.finalDataset,  ax=axes[1])
		axes[1].set_title('V4 vs Class Positive Correlation')


		sns.boxplot(x="Class", y="V2", data=self.finalDataset,  ax=axes[2])
		axes[2].set_title('V2 vs Class Positive Correlation')


		sns.boxplot(x="Class", y="V19", data=self.finalDataset,  ax=axes[3])
		axes[3].set_title('V19 vs Class Positive Correlation')

		plotPath = self.getPlotsFolder() / 'positiveCorrelations.png'
		plt.savefig(str(plotPath))
		st.image(str(plotPath))

	def saveDataset(self):
		self.finalDataset.to_csv(INPUT_PATH / 'correctedDataset.csv')