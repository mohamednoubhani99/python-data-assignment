import numpy as np
import itertools
import matplotlib.pyplot as plt


class PlotUtils:
    @staticmethod
    def plot_confusion_matrix(confusionMatrix, classes, title, normalize=False, colorMap=plt.cm.Blues):
        title = 'Confusion Matrix of {}'.format(title)

        if normalize:
            confusionMatrix = confusionMatrix.astype(float) / confusionMatrix.sum(axis=1)[:, np.newaxis]

        plt.imshow(confusionMatrix, interpolation='nearest', cmap=colorMap)
        plt.title(title)
        plt.colorbar()
        tick_marks = np.arange(len(classes))
        plt.xticks(tick_marks, classes, rotation=45)
        plt.yticks(tick_marks, classes)

        format_ = '.2f' if normalize else 'd'
        thresh = confusionMatrix.max() / 2.
        for i, j in itertools.product(range(confusionMatrix.shape[0]), range(confusionMatrix.shape[1])):
            plt.text(
                j,
                i,
                format(confusionMatrix[i, j], format_),
                horizontalalignment='center',
                color='white' if confusionMatrix[i, j] > thresh else 'black'
            )

        plt.tight_layout()
        plt.ylabel('True label')
        plt.xlabel('Predicted label')
