import pandas as pd


class EstimatorInterface:
    def fit(self, x_train, y_train):
        pass

    def predict(self, x_test):
        pass

    @staticmethod
    def save(model, path= 'model.joblib'):
        pass

    @staticmethod
    def load(model_path):
        pass
