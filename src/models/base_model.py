import numpy as np
from joblib import dump, load

from src.models.estimator_interface import EstimatorInterface


class BaseModel(EstimatorInterface):
    def __init__(self, model= None):
        self.model = model

    def fit(self, x_train, y_train):
        return self.model.fit(x_train, y_train)

    def predict(self, x_test):
        return self.model.predict(x_test)

    def load(self, model_path):
        model = load(model_path)
        self.model = model

    @staticmethod
    def save(model, path= 'model.joblib'):
        dump(model, path)
