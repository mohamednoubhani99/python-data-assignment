from PIL import Image
from src.constants import CM_PLOT_PATH,CORRECTED_DATASET_PATH
from src.training.train_pipeline import TrainingPipeline
import time
import streamlit as st
class trainer:
	def __init__(self):
		self.serialize = False
	def run(self,name,plotName):
		with st.spinner('Training model, please wait...'):
			time.sleep(1)
			try:
				trainingPipline = TrainingPipeline(dataPath=CORRECTED_DATASET_PATH)
				trainingPipline.train(serialize=self.serialize, modelName=name)
				trainingPipline.render_confusion_matrix(plot_name=plotName)
				accuracy, f1 = trainingPipline.get_model_perfomance()
				col1, col2 = st.columns(2)

				col1.metric(label="Accuracy score", value=str(round(accuracy, 4)))
				col2.metric(label="F1 score", value=str(round(f1, 4)))
				st.image(Image.open(CM_PLOT_PATH))

			except Exception as e:
				st.error('Failed to train model!')
				st.exception(e)

	def setSerialize(self):
		self.serialize = True