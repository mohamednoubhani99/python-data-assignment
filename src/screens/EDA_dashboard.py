import streamlit as st
from src.eda_backend import analyse

class edaDashboard:
    def __init__(self):
        self.header ="Exploratory Data Analysis"
        self.information = "In this section, you are invited to create insightful graphs "
        "about the card fraud dataset that you were provided."
        st.header(self.header)
        st.info(self.information)
        self.analyser = analyse()

    def draw(self):
        st.title('dataset overview')
        st.write(self.analyser.dataset.head(5))
        st.markdown('our dataset consists of 28 unknown features' 
            +' with an Amount field containing the amount of the transaction '
            +'and a calss indicating if it was a fraud or not ')
        st.title('dataset description')
        st.dataframe(self.analyser.dataset.describe())
        st.markdown('- The transaction amount is relatively small. The mean of'
            +'all the mounts made is approximately  88 Dh.')
        st.markdown('- Most of the transactions were Non Fraudulent (99.83%) of the time,'
            +' while Fraud transactions occurs (0.17%) of the time in the dataframe.')
        st.markdown('- There are no "Null" values, so we don\'t have to work on ways'
            + ' to replace values. while Fraud transactions occurs (0.17%) of the time '
            +'in the dataframe.')
        st.markdown('- None Fraudulent transactions make' 
         +str(round(self.analyser.dataset['Class'].value_counts()[0]/len(self.analyser.dataset) * 100,2)) + 'of the dataset'
         + ' while only' + str(round(self.analyser.dataset['Class'].value_counts()[1]/len(self.analyser.dataset) * 100,2)) 
         + ' is fraudulent.')
        st.title('study of Distributions')
        self.analyser.drawColumnChart()
        st.markdown('with 0 being clear and 1 being Fraudulent ,this distribution graph shows how unbalanced'
            ' our data is. There are multiple ways in which we can reduce this bias , which we will be '
            +'implementing here.')
        self.analyser.drawAmountDistribution()
        self.analyser.drawTimeDistribution()
        st.title('Fixing the Dataset')
        st.markdown('We will scale the columns (comprise of Time and Amount) , they should be scaled'
            +' as the other columns. On the other hand, we need to also create a sub sample of the dataframe in '
            +'order to have an equal amount of Fraud and Non-Fraud cases, helping our algorithms better understand '
            +'patterns that determines whether a transaction is a fraud or not.')
        st.markdown('our Sub sample will be a new dataframe with a 1:1 ratio of fraud and non fraud transactions')
        st.markdown('by doing so we fix the imbalance in our original dataset that would otherwise face : ')
        st.markdown('-Overfitting: Our classification models will assume that in most cases there are no frauds! '
            +'What we want for our model is to be certain when a fraud occurs.')
        st.markdown('-Wrong Correlations: Although we don\'t know what the "V" features stand for, it will be useful'
        +' to understand how each of this features influence the result (Fraud or No Fraud) by having an imbalance ' 
        +'dataframe we are not able to see the true correlations between the class and features.')
        st.markdown('There are 492 cases of fraud in our dataset so we can randomly get 492 cases of non-fraud to create our new sub dataframe.')
        self.analyser.scaleData()
        st.title('The new scalled dataset')
        st.write(self.analyser.dataset.head(5))
        self.analyser.varryingDataset()
        st.title('The new reballanced dataset')
        st.markdown('In this phase we will implement *"Random Under Sampling"* which basically consists of removing data'
            +' in order to have a more balanced dataset and thus avoiding our models to overfitting.\n Steps:')
        st.markdown('- The first thing we have to do is determine how imbalanced is our class ')
        st.markdown('- Once we determine how many instances are considered fraud transactions (Fraud = "1") , we should '
            +'bring the non-fraud transactions to the same amount as fraud transactions (assuming we want a 50/50 ratio),'
            +' this will be equivalent to 492 cases of fraud and 492 cases of non-fraud transactions.')
        st.write(self.analyser.finalDataset.head(5))
        st.markdown('Note: The main issue with "Random Under-Sampling" is that we run the risk that our classification models'
        +' will not perform as accurate as we would like to since there is a great deal of information loss (bringing 492 '
        +'non-fraud transaction from 284,315 non-fraud transaction) ')
        st.title('Distribution and Correlation')
        st.markdown('Now that we have our dataframe correctly balanced')
        self.analyser.drawNewDistribution()
        st.markdown('Correlation matrices are the essence of understanding our data. We want to know if there are features that'
        +' influence heavily in whether a specific transaction is a fraud. However, it is important that we use the correct '
        +'dataset (subsample) in order for us to see which features have a high positive or negative correlation with regards '
        +'to fraud transactions.')
        self.analyser.drawCorrelationMatrix()
        st.markdown('study of the negative correlations:')
        self.analyser.drawNegativeCorrelations()
        self.analyser.drawPositiveCorrelations()
        st.markdown('- Negative Correlations: V17, V14, V12 and V10 are negatively correlated. Notice how the lower these values'
            +' are, the more likely the end result will be a fraud transaction.')
        st.markdown('- Positive Correlations: V2, V4, V11, and V19 are positively correlated. Notice how the higher these values' 
            +' are, the more likely the end result will be a fraud transaction.')
        self.analyser.saveDataset()

