import time
import streamlit as st
import requests
from src.constants import INFERENCE_EXAMPLE
from src.inference_backend import inference 

class inferenceDashbord:
    def __init__(self,header="",information="",inferneceEndPoint="",specificFeatures=False):

        if header != "":
            self.header = header
        else:
            self.header = "Fraud Inference"

        if information != "":
            self.information = information
        else:    
            self.information = "This section simplifies the inference process. "
            "You can tweak the values of feature 1, 2, 19, "
            "and the transaction amount and observe how your model reacts to these changes."
        if inferneceEndPoint != "":
            self.inferneceEndPoint = inferneceEndPoint    
        else:                  
            self.inferneceEndPoint = 'http://localhost:5000//api/inference'

        st.header(self.header)
        st.info(self.information)
        self.features = self.addFeatures(specificFeatures)
        self.amount = st.number_input('Transaction Amount', value=1000, min_value=0, max_value=int(1e10), step=100)
        self.infer = st.button('Run Fraud Inference')
        self.inference = inference()
    def setHeader(self,header):
        self.header = header 
        st.header(self.header)

    def setInformation(self,information):
        self.header = header 
        st.header(self.header)

    def addFeatures(self,specificFeatures):
        features = []
        if specificFeatures==False:
            
            for index in range(28):
                features.append(st.slider('Transaction Feature ' + str(index), -10.0, 10.0, step=0.01, value=0.0))
        else:
            #if self.isNumbers(specificFeatures):
            for feature in specificFeatures:
                features.append(st.slider('Transaction Feature ' + str(feature), -10.0, 10.0, step=0.01, value=0.0))
       
        return features

    def listen(self):
        if self.infer:
            data = []
            for index in range(len(self.features)):
                data.append(self.features[index])
            data.append(self.amount)
            self.inference.getData(data)
            self.inference.runInference(self.inferneceEndPoint)

    