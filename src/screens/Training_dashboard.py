
import streamlit as st

from src.training_backend import trainer
class trainingDashboard:
    def __init__(self,header="",information="",modelName=""):
        if header != "":
            self.header = header
        else:
            self.header = "Model Training"

        if information != "":
            self.information = information
        else:    
            self.information = "Before you proceed to training your model. Make sure you have checked your training pipeline code and that it is set properly."
        if modelName != "":
            self.modelName = modelName
        else:  
            self.modelName =  'decision_tree'
        st.header(self.header)
        st.info(self.information)
        self.name = st.text_input('Model name', placeholder=self.modelName)
        self.train = st.button('Train Model')
        self.save = st.checkbox('save the model')
        self.trainer = trainer()
    def listen(self):
        if self.save:
            self.trainer.setSerialize()
        if self.train:
            self.trainer.run(self.name,self.name)



