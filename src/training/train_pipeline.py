import pandas as pd
import matplotlib.pyplot as plt
#add Flask-SQLAlchemy flask-sqlalchemy
from sklearn.metrics import accuracy_score, f1_score, confusion_matrix
from sklearn.model_selection import train_test_split
import xgboost as xgb

from src.constants import AGGREGATOR_MODEL_PATH, CORRECTED_DATASET_PATH, CM_PLOT_PATH
from src.models.aggregator_model import AggregatorModel
from src.models.decision_tree_model import DecisionTreeModel
from src.utils import PlotUtils


class TrainingPipeline:
    def __init__(self,dataPath):
        try:
            dataset = pd.read_csv(dataPath)
            X, y = dataset.iloc[:,1:-1],dataset.iloc[:,-1]
            data_dmatrix = xgb.DMatrix(data=X,label=y)
            self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(
                X,
                y, 
                test_size=0.2, 
                random_state=42)#0.9368 20

            self.model = None
        except (UnboundLocalError,FileNotFoundError) as e:
            print('Failed to locate dataset!')

    def train(self, serialize= True, modelName ='model.joblib'):
        xg_reg = xgb.XGBClassifier(objective ='reg:squarederror', colsample_bytree = 0.3, learning_rate = 0.1,
            max_depth = 5, alpha = 10, n_estimators = 10)

        self.model = AggregatorModel(models=[xg_reg])

        self.model.fit(
            self.x_train,
            self.y_train
        )

        model_path = str(AGGREGATOR_MODEL_PATH).replace('aggregator_model', modelName)
        if serialize:
            AggregatorModel.save(
                self.model,
                model_path
            )

    def get_model_perfomance(self):
        predictions = self.model.predict(self.x_test)
        return accuracy_score(self.y_test, predictions), f1_score(self.y_test, predictions)

    def render_confusion_matrix(self, plot_name= 'plot.png'):
        predictions = self.model.predict(self.x_test)
        cm = confusion_matrix(self.y_test, predictions, labels=[0, 1])
        plt.rcParams['figure.figsize'] = (6, 6)

        PlotUtils.plot_confusion_matrix(
            cm,
            classes=['Clear(0)', 'Fraudulent(1)'],
            normalize=False,
            title='Current Model'
        )

        plot_path = str(CM_PLOT_PATH)
        plt.savefig(plot_path)
        plt.show()


if __name__ == "__main__":
    trainingPipline = TrainingPipeline()
    trainingPipline.train(serialize=True)
    accuracy, f1 = trainingPipline.get_model_perfomance()
    trainingPipline.render_confusion_matrix()
    print("ACCURACY = {}, F1 = {}".format(accuracy, f1))
