import time
import streamlit as st
import requests
from src.constants import INFERENCE_EXAMPLE

class inference:
	def __init__(self):
		self.exemple = INFERENCE_EXAMPLE
	def runInference(self,inferneceEndPoint):
		with st.spinner('Running inference...'):
			time.sleep(1)
			try:
				result = requests.post(
				inferneceEndPoint,
				json=self.exemple
				)
				if int(result.text) == 1:
					st.success('Done!')
					st.metric(label="Status", value="Transaction: Fraudulent")
				else:
					st.success('Done!')
					st.metric(label="Status", value="Transaction: Clear")
			except Exception as e:
				st.error('Failed to call Inference API!')
				st.exception(e)

	def getData(self,data):
		self.exemple[0] = data[0]
		self.exemple[1] = data[1]
		self.exemple[18] = data[2]
		self.exemple[28] = data[3]
		#10 12 15 27
		