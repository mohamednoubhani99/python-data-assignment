# Data assignement

This is my imporved version of [assignment](https://gitlab.com/octomaroc_stage2022/python-data-assignment)

## Installation

Use the same instructions as for the oldr repository to install all dependencies.

```bash
cd python-data-assignement
```
```bash
pipenv install
```
```bash
pipenv shell
```
start the application 
```bash
 sh run.sh
```
```bash
python -m streamlit run dashboard.py
```
- API : http://localhost:5000

- Streamlit Dashboard : http://localhost:8000

## Assignment
### 1 - Code Refactoring
for this part , I've tried to split the code found in dashboard.py into multiple files for front_end (found under src/screens folder with dashboard in their name) and backend (found under src folder with backend in their name),while opting an OOP paradigm.
I've also changed the naming convention from random assignments into phrases with the first letter of each word capitalized (ie : myVariableName) except for the test functions that have an "_" seperating each word (ie : my_test_function) and so the does the file naming.
I've also eliminated the necessary condition checks and the bad code parts. 
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/94a43c1d71a23949d6e0e9fd5a383e054087a855/images/Screenshot%20from%202021-12-08%2023-22-21.png)

### 2 - Exploratory Data Analysis
In this section I've analysed the data provided and found a major problem of imbalance,since one class makes almost the entire dataset with over 99.83% 
 ![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-09-45.png)
 ![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-09-48.png)
 ![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-09-50.png)
 ![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-09-55.png)
 ![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-09-58.png)
I've fixed the issue by undersamping.
 ![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-10-02.png)
 ![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-10-05.png)
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-10-09.png)
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-10-15.png)
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-10-18.png)
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-10-21.png)
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-10-23.png)

## 3 - Training
for the training I've used the new balanced dataset on a XGBoost model using the same training piple and got better results on the F1 score
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-10-29.png)
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-11-49.png)
## 4 - Inference
for the inference I've add a function to control either which features to be shown or how many random features to be displayed.
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-12-06.png)
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-12-09.png)
## 5 - Unit testing
for the tests I've made some functions to test the behavior of the backend functions and to also test for the persistence of important files.
![This is an image](https://gitlab.com/mohamednoubhani99/python-data-assignment/-/raw/main/images/Screenshot%20from%202021-12-08%2023-12-53.png)
## License
[MIT](https://choosealicense.com/licenses/mit/)
