import streamlit as st
from src.screens.EDA_dashboard import edaDashboard
from src.screens.Training_dashboard import trainingDashboard
from src.screens.inference_dashboard import inferenceDashbord


class MainDashBoard:
    def __init__(self,title="",sideBarTitle=""):
        self.view = None
        if title != "":
            self.title = title
        else:
            self.title = "Card Fraud Detection Dashboard"
        if sideBarTitle != "":
            self.sideBarTitle = sideBarTitle
        else:
            self.sideBarTitle = "Data Themes"

        st.title(self.title)
        st.sidebar.title(self.sideBarTitle)
        self.sidebar_options = st.sidebar.selectbox(
            "Options",
            ("EDA", "Training", "Inference")
        )
        
    def listen(self):
        if self.sidebar_options == "EDA":
            self.view = edaDashboard()
            self.view.draw()

        elif self.sidebar_options == "Training":
            self.view = trainingDashboard()
            self.view.listen()
         
        else:
            self.view = inferenceDashbord(specificFeatures=[11,13,15])
            self.view.listen()

if __name__ == "__main__":
    mainDashboard = MainDashBoard()
    mainDashboard.listen()
    