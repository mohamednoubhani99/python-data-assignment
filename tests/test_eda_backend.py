import pytest
from src.eda_backend import analyse
import os
import pandas as pd 
analyser = analyse()

def test_if_Data_Exist():
	assert(os.path.exists(analyser.getDataset()))

def test_if_Dataset_is_balanced():
	dataset = analyser.getDataset()
	
def test_if_ColumnChart_is_Drawn():
	analyser.drawColumnChart()
	path = analyser.getPlotsFolder() /'ColumnChart.png'
	assert(os.path.isfile(path))

def test_if_amount_distribution_is_created():
	analyser.drawAmountDistribution()
	path = analyser.getPlotsFolder() / 'AmountDistribution.png'
	assert(os.path.isfile(path))

def test_if_Time_Distribution_is_created():
	analyser.drawTimeDistribution()
	path = analyser.getPlotsFolder() /'TimeDistribution.png'
	assert(os.path.isfile(path))

def test_if_data_is_scaled():
	try:
		analyser.scaleData()
	except MyError:
		pytest.fail("Unexpected error .."+str(MyError))

def test_if_data_is_varried():
	try:
		analyser.varryingDataset()
	except MyError:
		pytest.fail("Unexpected error .."+str(MyError))
	try:
		analyser.finalDataset
	except NameError as err: 
		pytest.fail("Unexpected error .."+str(err))

def test_if_New_Distribution_is_created():
	analyser.drawNewDistribution()
	path = analyser.getPlotsFolder() / 'newDistribution.png'
	assert(os.path.isfile(path))


def test_if_Correlation_Matrix_is_created():
	analyser.drawCorrelationMatrix()
	path = analyser.getPlotsFolder() / 'oldCorrelationMatrix.png'
	assert(os.path.isfile(path))

def test_if_Correlation_Matrix_is_created():
	analyser.drawCorrelationMatrix()
	pathToOldCorrelaction = analyser.getPlotsFolder() / 'oldCorrelationMatrix.png'
	pathToNewCorrelaction = analyser.getPlotsFolder() / 'newCorrelationMatrix.png'
	assert(os.path.isfile(pathToOldCorrelaction))
	assert(os.path.isfile(pathToNewCorrelaction))

def test_if_Negative_Correlations_is_created():
	analyser.drawNegativeCorrelations()
	path = analyser.getPlotsFolder() / 'negativeCorrelations.png'
	assert(os.path.isfile(path))

def drawPositiveCorrelations():
	analyser.drawNegativeCorrelations()
	path = analyser.getPlotsFolder() / 'positiveCorrelations.png'
	assert(os.path.isfile(path))