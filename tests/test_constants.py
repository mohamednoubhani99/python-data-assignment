import pytest
from src.constants import *
import os

def test_INPUT_PATH():
	assert(os.path.isdir(INPUT_PATH))

def test_DATASET_PATH():
	assert(os.path.exists(DATASET_PATH))

def test_CORRECTED_DATASET_PATH():
	assert(os.path.exists(CORRECTED_DATASET_PATH))

def test_OUTPUT_PATH():
	assert(os.path.isdir(OUTPUT_PATH))

def test_DECISION_TREE_MODEL_PATH():
	assert(os.path.isfile(DECISION_TREE_MODEL_PATH))

def test_MODEL_PATH():
	assert(os.path.isdir(MODEL_PATH))

def test_AGGREGATOR_MODEL_PATH():
	assert(os.path.exists(AGGREGATOR_MODEL_PATH))

def test_CORRECTED_MODEL_PATH():
	assert(os.path.exists(CORRECTED_MODEL_PATH))

def test_CM_PLOT_PATH():
	assert(os.path.exists(CM_PLOT_PATH))
